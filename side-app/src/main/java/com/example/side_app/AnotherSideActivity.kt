package com.example.side_app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class AnotherSideActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.another_activity_side)
    }
}
